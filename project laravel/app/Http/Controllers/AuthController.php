<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register()
    {
        return view('register');
    }

    // public function send(request $request)
    // {
    //     dd($request);
    // }
    public function send(request $request)
    {
        $namaDepan = $request['fname'];
        $namaBelakang = $request['lname'];
        // return "test";
        return view('welcome', ['namaDepan' => $namaDepan, 'namaBelakang' => $namaBelakang]);
    }
}
