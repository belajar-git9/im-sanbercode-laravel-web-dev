<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form Pendaftaran</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/kirim" method="post">
        @csrf 
        <label>First name:</label> <br> <br>
        <input type="text"> <br> <br>
        <label>Last name:</label> <br> <br>
        <input type="text"> <br> <br>
        <label>Gender:</label> <br> <br>
        <input type="radio" name="gender"> Male <br>
        <input type="radio" name="gender"> Female <br>
        <input type="radio" name="gender"> Other <br> <br>
        <label>Nationality:</label> <br> <br>
        <select name=""> <br>
            <option value="">Indonesian</option>
            <option value="">Singapore</option>
            <option value="">Malaysia</option>
            <option value="">Australian</option>
        </select> <br> <br>
        <label>Language Spoken:</label> <br> <br>
        <input type="checkbox"> Bahasa Indonesia <br>
        <input type="checkbox"> English <br>
        <input type="checkbox"> Other <br> <br>
        <label>Bio:</label> <br> <br>
        <textarea cols="20" rows="10"></textarea> <br>
        <input type="submit" value="Sign Up">
    </form>

</body>
</html>