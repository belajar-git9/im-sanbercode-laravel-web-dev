<?php

require('animal.php');
require('ape.php');
require('frog.php');

$sheep = new Animal("shaun");

echo "Name : $sheep->name <br>"; // "shaun"
echo "Legs : $sheep->legs <br>"; // 4
echo "cold clooded : $sheep->cold_blooded <br> <br>"; // "no"

$sungokong = new Ape("kera sakti");

echo "Name : $sungokong->name <br>";
echo "Legs : $sungokong->legs <br>";
echo "cold clooded : $sungokong->cold_blooded <br>";
$sungokong->yell();
echo "<br> <br>";

$kodok = new Frog("buduk");

echo "Name : $kodok->name <br>";
echo "Legs : $kodok->legs <br>";
echo "cold clooded : $kodok->cold_blooded <br>";
$kodok->jump() ; // "hop hop"